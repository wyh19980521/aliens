import pygame
from pygame.sprite import Sprite
class Alien(Sprite):
    '''表示外星人的类'''
    def __init__(self,ai_settings,screen):
        super(Alien,self).__init__()
        self.screen =screen
        self.ai_settings =ai_settings
        #加载外星人图像
        self.image=pygame.image.load('images/alien.bmp')
        self.rect = self.image.get_rect()
        #初始在左上角附近
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height


        self.x = float(self.rect.x)
    def check_edges(self):
        """Return True if alien is at edge of screen."""
        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right:
            return True
        elif self.rect.left <= 0:
            return True
    def update(self):
        self.x += (self.ai_settings.alien_speed_factor *  self.ai_settings.fleet_direction)
        self.rect.x=self.x
        '''
        每次更新外星人位置时，都将它向右移动，移动量为alien_speed_factor的值。我们使用属
性self.x跟踪每个外星人的准确位置，这个属性可存储小数值（见Ø）。然后，我们使用self.x
的值来更新外星人的rect的位置（见）。
        '''
    def blitme(self):
        self.screen.blit(self.image,self.rect)