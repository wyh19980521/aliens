import sys
import pygame
from pygame.constants import KEYDOWN, QUIT
from bullet import Bullet
from alien import Alien
from time import sleep
def check_events(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
         check_keydown_events(event,ai_settings,screen,ship,bullets=bullets)               
        elif event.type == pygame.KEYUP:
         check_keyup_events(event,ship)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x,mouse_y=pygame.mouse.get_pos()
            check_play_button(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y)
def check_play_button(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y):
    button_clicked = play_button.rect.collidepoint(mouse_x, mouse_y)
    if button_clicked and not stats.game_active:
        stats.reset_starts()
        stats.game_active = True
        pygame.mouse.set_visible(False)
        # 重置记分牌图像
        sb.prep_score()
        sb.prep_high_score()
        sb.prep_level()
        sb.prep_ships()
        aliens.empty() 
        bullets.empty()
        create_fleet(ai_settings, screen, ship, aliens) 
        #重置游戏初始化配置
        ai_settings.initialize_dynamic_settings()
        ship.center_ship()
def check_keydown_events(event,ai_settings,screen,ship,bullets):
    '''
    我们在函数check_events()中包含形参ship，因为玩家按右箭头键时，需要将飞船向右移动。
在函数check_events()内部，我们在事件循环中添加了一个elif代码块，以便在Pygame 检测到
KEYDOWN事件时作出响应（见）。我们读取属性event.key，以检查按下的是否是右箭头键
（pygame.K_RIGHT）（见）。如果按下的是右箭头键，就将ship.rect.centerx的值加1，从而将飞
船向右移动（见）
    '''
    if event.key == pygame.K_RIGHT:
      ship.moving_right = True
    if event.key == pygame.K_LEFT:
       ship.moving_left = True
    if event.key == pygame.K_UP:
       ship.moving_up = True
    if event.key == pygame.K_DOWN:
       ship.moving_down = True
    elif event.key == pygame.K_SPACE:
        fire_bullet(ai_settings,screen,ship,bullets)
    elif event.key == pygame.K_q:
        sys.exit()  
def check_keyup_events(event,ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    if event.key == pygame.K_LEFT:
        ship.moving_left = False
    if event.key == pygame.K_UP:
       ship.moving_up = False
    if event.key == pygame.K_DOWN:
       ship.moving_down = False
def create_fleet(ai_settings,screen,ship,aliens):
    """创建外星人群""" 
 # 创建一个外星人，并计算一行可容纳多少个外星人
 # 外星人间距为外星人宽度
    alien =Alien(ai_settings,screen)
    '''
    这些代码大都在前面详细介绍过。为放置外星人，我们需要知道外星人的宽度和高度，因此
在执行计算前，我们先创建一个外星人（见Ø）。这个外星人不是外星人群的成员，因此没有将
它加入到编组aliens中。在处，我们从外星人的rect属性中获取外星人宽度，并将这个值存储
到alien_width中，以免反复访问属性rect。在处，我们计算可用于放置外星人的水平空间，以
及其中可容纳多少个外星人。
相比于前面介绍的工作，这里唯一的不同是使用了int()来确保计算得到的外星人数量为整
数（见），因为我们不希望某个外星人只显示一部分，而且函数range()也需要一个整数。函数
int()将小数部分丢弃，相当于向下圆整（这大有裨益，因为我们宁愿每行都多出一点点空间，
也不希望每行的外星人之间过于拥挤）。
接下来，我们编写了一个循环，它从零数到要创建的外星人数（见）。在这个循环的主体
中，我们创建一个新的外星人，并通过设置x坐标将其加入当前行（见）。将每个外星人都往右
推一个外星人的宽度。接下来，我们将外星人宽度乘以2，得到每个外星人占据的空间（其中包
括其右边的空白区域），再据此计算当前外星人在当前行的位置。最后，我们将每个新创建的外
星人都添加到编组aliens中。
    '''
    number_aliens_x =get_number_aliens_x(ai_settings,alien.rect.width)
    number_row = get_number_rows(ai_settings,ship.rect.height,alien.rect.height)
    '''
    为计算屏幕可容纳多少行外星人，我们在函数get_number_rows()中实现了前面计算
available_space_y和number_rows的公式（见Ø），这个函数与get_number_aliens_x()类似。计算
公式用括号括起来了，这样可将代码分成两行，以遵循每行不超过79字符的建议（见）。这里
使用了int()，因为我们不想创建不完整的外星人行。
为创建多行，我们使用两个嵌套在一起的循环：一个外部循环和一个内部循环（见）。其
中的内部循环创建一行外星人，而外部循环从零数到要创建的外星人行数。Python将重复执行创
建单行外星人的代码，重复次数为number_rows。
为嵌套循环，我们编写了一个新的for循环，并缩进了要重复执行的代码。（在大多数文本编
辑器中，缩进代码块和取消缩进都很容易，详情请参阅附录B。）我们调用create_alien()时，传
递了一个表示行号的实参，将每行都沿屏幕依次向下放置。
create_alien()的定义需要一个用于存储行号的形参。在create_alien()中，我们修改外星
人的y坐标（见），并在第一行外星人上方留出与外星人等高的空白区域。相邻外星人行的y坐
标相差外星人高度的两倍，因此我们将外星人高度乘以2，再乘以行号。第一行的行号为0，因此
第一行的垂直位置不变，而其他行都沿屏幕依次向下放置。
在 create_fleet() 的定义中，还新增了一个用于存储 ship 对象的形参，因此在
alien_invasion.py中调用create_fleet()时，需要传递实参ship：
    
    '''
    
    for row_number in range(number_row):
        for alien_number in range(number_aliens_x):
         create_alien(ai_settings,screen,aliens,alien_number,row_number)
    
def get_number_aliens_x(ai_settings,alien_width):
    available_space_x = ai_settings.screen_width - 2 * alien_width  
    number_aliens_x=int(available_space_x / ( 2 * alien_width))
    return number_aliens_x
def create_alien(ai_settings,screen,aliens,alien_number,row_number):
    alien = Alien(ai_settings,screen)
    alien_width=alien.rect.width
    alien.x = alien_width + 2 * alien_width * alien_number
    alien.rect.x=alien.x
    alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
    aliens.add(alien)
def get_number_rows(ai_setting,ship_height,alien_height):
    available_space_y = (ai_setting.screen_height -( 3 * alien_height)- ship_height)
    number_rows = int(available_space_y/( 2 * alien_height))
    return number_rows

def update_screen(ai_settings,screen,stats,sb,ship,aliens,bullets,play_button):
    screen.fill(ai_settings.bg_color)
    for bullet in bullets:
        bullet.draw_bullet()
    sb.show_score()
    ship.blitme()
    aliens.draw(screen)
    if not stats.game_active:
        play_button.draw_button()
    pygame.display.flip()
def update_aliens(ai_settings,stats, sb,screen, ship, aliens, bullets):
    check_fleet_edgs(ai_settings,aliens)
    aliens.update()
    if pygame.sprite.spritecollideany(ship,aliens):
        ship_hit(ai_settings,stats,sb,screen,ship,aliens,bullets)
    check_aliens_bottom(ai_settings,stats,sb,screen,ship,aliens,bullets)
def check_aliens_bottom(ai_settings,stats,sb,screen,ship,aliens,bullets):
    '''检查是否有外星人到达底部'''
    screen_rect =screen.get_rect()
    for alien in aliens.sprites():
        if alien.rect.bottom >= screen_rect.bottom:
            ship_hit(ai_settings,stats,sb,screen,ship,aliens,bullets)
            break
def ship_hit(ai_settings, stats,sb, screen, ship, aliens, bullets):
    '''响应被外星人撞到的飞机'''
    if stats.ships_left > 0:
      stats.ships_left -= 1
      aliens.empty()
      bullets.empty()
      sb.prep_ships()
    #创建新的外星人
      create_fleet(ai_settings,screen,ship,aliens)
      ship.center_ship()
    #暂停
      sleep(0.5)
    else:
        stats.game_active = False
        #光标出现
        pygame.mouse.set_visible(True)
def check_fleet_edgs(ai_settings,aliens):
    for alien in aliens.sprites():
        if alien.check_edges():
            change_fleet_direction(ai_settings,aliens)
            break
def change_fleet_direction(ai_settings,aliens):
    for alien in aliens.sprites():
        alien.rect.y += ai_settings.fleet_drop_speed
    ai_settings.fleet_direction *= -1
def fire_bullet(ai_settings,screen,ship,bullets):
    '''
    开火函数
    '''
    if len(bullets) < ai_settings.bullets_allowed:
       #生成一个新的子弹放入子弹租
        new_bullet = Bullet(ai_settings,screen,ship)
        bullets.add(new_bullet) 
def update_bullets(ai_settings, screen, stats,sb,ship, aliens, bullets):
    for bullet in bullets.copy(): 
      if bullet.rect.bottom <= 0: 
       bullets.remove(bullet)
    check_bullet_alien_collisions(ai_settings, screen,stats,sb,ship, aliens, bullets)
def check_bullet_alien_collisions(ai_settings, screen,stats,sb, ship, aliens, bullets):
    """响应子弹和外星人的碰撞""" 
    # 删除发生碰撞的子弹和外星人
    collisions = pygame.sprite.groupcollide(bullets, aliens, True, True)
    if collisions:
        for aliens in collisions.values():
         '''
         在check_bullet_alien_collisions()中，与外星人碰撞的子弹都是字典collisions中的一个
键；而与每颗子弹相关的值都是一个列表，其中包含该子弹撞到的外星人。我们遍历字典
collisions，确保将消灭的每个外星人的点数都记入得分：
         '''
         stats.score += ai_settings.alien_points
         sb.prep_score()
        check_high_score(stats,sb)
    if len(aliens) == 0: 
 # 删除现有的所有子弹，并创建一个新的外星人群
     #加快游戏难度
     stats.level +=1
     sb.prep_level()
     ai_settings.increase_speed()
     bullets.empty() 
     create_fleet(ai_settings, screen, ship, aliens)
def remove_bullet(bullets):
    for bullet in bullets.copy():
        if bullet.rect.bottom <=0:
            bullets.remove(bullet)
def check_high_score(stats, sb):
    if stats.score > stats.high_score:
        stats.high_score = stats.score
        sb.prep_high_score()    