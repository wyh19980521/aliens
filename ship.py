import pygame 
from pygame.sprite import Sprite
class Ship(Sprite):
    def __init__(self,ai_settings,screen):
        #初始化飞船并设置其位置
        super(Ship, self).__init__()
        self.screen = screen
        # 加载飞船图像并获取其外接矩形
        self.image = pygame.image.load('images/ship.bmp')
        self.ai_settings = ai_settings
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        #将新飞船放在屏幕底部中间
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        #飞船的设置
        #移动标志
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False
         # 在飞船的属性center中存储小数值
        self.center = float(self.rect.centerx)
        self.bottom = float(self.rect.bottom)
    def update(self):
        '''
        我们添加了标志self.moving_left；在方法update()中，我们添加了一
个if代码块而不是elif代码块，这样如果玩家同时按下了左右箭头键，将先增大飞船的
rect.centerx值，再降低这个值，即飞船的位置保持不变。如果使用一个elif代码块来处理向左
移动的情况，右箭头键将始终处于优先地位。从向左移动切换到向右移动时，玩家可能同时按住
左右箭头键，在这种情况下，前面的做法让移动更准确。
        '''
        if self.moving_right and self.rect.right < self.screen_rect.right:
           self.center += self.ai_settings.ship_speed_factor
        if self.moving_left and self.rect.left >0 :
            self.center -= self.ai_settings.ship_speed_factor
        self.rect.centerx = self.center
        if self.moving_up and self.rect.top > 0 :
           self.bottom -= self.ai_settings.ship_speed_factor
        if self.moving_down and self.rect.bottom< self.screen_rect.bottom:
            self.bottom += self.ai_settings.ship_speed_factor
        self.rect.bottom = self.bottom

    def blitme(self):
        self.screen.blit(self.image,self.rect)
    '''
    首先，我们导入了模块pygame。Ship的方法__init__()接受两个参数：引用self和screen，
其中后者指定了要将飞船绘制到什么地方。为加载图像，我们调用了pygame.image.load()（见）。
这个函数返回一个表示飞船的surface，而我们将这个surface存储到了self.image中。
加载图像后，我们使用get_rect()获取相应surface的属性rect（见）。Pygame的效率之所以
如此高，一个原因是它让你能够像处理矩形（rect对象）一样处理游戏元素，即便它们的形状并
非矩形。像处理矩形一样处理游戏元素之所以高效，是因为矩形是简单的几何形状。这种做法的
效果通常很好，游戏玩家几乎注意不到我们处理的不是游戏元素的实际形状。
处理rect对象时，可使用矩形四角和中心的x和y坐标。可通过设置这些值来指定矩形的位置。
要将游戏元素居中，可设置相应rect对象的属性center、centerx或centery。要让游戏元素
与屏幕边缘对齐，可使用属性top、bottom、left或right；要调整游戏元素的水平或垂直位置，
可使用属性x和y，它们分别是相应矩形左上角的x和y坐标。这些属性让你无需去做游戏开发人员
原本需要手工完成的计算，你经常会用到这些属性。
    
    '''
    def center_ship(self): 
      """让飞船在屏幕上居中""" 
      self.center = self.screen_rect.centerx
