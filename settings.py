class Settings():
    '''存储外星人侵略所有设置的类'''
    def __init__(self):
        '''初始化游戏配置'''
        self.screen_width= 1200
        self.screen_height = 800
        self.bg_color =(230,230,230)
        self.ship_speed_factor = 1.5
        #子弹设置
        self.bullet_speed_factor = 2
        self.bullet_width = 200
        self.bullet_height =15
        self.bullet_color = (60,60,60)
        self.bullets_allowed = 4
        
        #外星人设置
        self.alien_speed_factor = 1 
        self.fleet_drop_speed = 10
        self.score_scale = 1.5
        #1表示右转 -1 表示左移
        '''设置fleet_drop_speed指定了有外星人撞到屏幕边缘时，外星人群向下移动的速度。将这个
速度与水平速度分开是有好处的，这样你就可以分别调整这两种速度了。
要实现fleet_direction设置，可以将其设置为文本值，如'left'或'right'，但这样就必须
编写if-elif语句来检查外星人群的移动方向。鉴于只有两个可能的方向，我们使用值1和1来表
示它们，并在外星人群改变方向时在这两个值之间切换。另外，鉴于向右移动时需要增大每个外
星人的x坐标，而向左移动时需要减小每个外星人的x坐标，使用数字来表示方向更合理。'''
        self.fleet_direction = 1 
        #飞船设置
        self.ship_limit = 3
        #加快游戏速度
        self.speedup_scale = 1.1
        self.initialize_dynamic_settings()
    def initialize_dynamic_settings(self): 
     """初始化随游戏进行而变化的设置""" 
     self.ship_speed_factor = 1.5 
     self.bullet_speed_factor = 3 
     self.alien_speed_factor = 1 
     self.alien_points = 50
     #每个外星人50分

      # fleet_direction为1表示向右；为-1表示向左
     self.fleet_direction = 1
    def increase_speed(self): 
      """提高速度设置""" 
      self.ship_speed_factor *= self.speedup_scale 
      self.bullet_speed_factor *= self.speedup_scale 
      self.alien_speed_factor *= self.speedup_scale
      self.alien_points = int(self.alien_points * self.score_scale)