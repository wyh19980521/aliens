import sys
import pygame 
from settings import Settings
from alien import Alien
from ship import Ship
import game_functions as gf
from pygame.sprite import Group
from game_stats import GameStats    
from button import Button
from scoreboard import Scoreboard
def run_game():
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_width,ai_settings.screen_height))
    pygame.display.set_caption('Alien invasion')
    bg_color=(ai_settings.bg_color)
    #创建飞船
    ship = Ship(ai_settings,screen)
    #创建外星人
    alien = Alien(ai_settings,screen)
    # 创建Play按钮
    play_button = Button(ai_settings, screen, "Play")
    #创建一个存储游戏信息的实例
    stats = GameStats(ai_settings)
    sb = Scoreboard(ai_settings, screen, stats)
    #创建存储子弹的数组
    bullets = Group()
    aliens = Group()
    gf.create_fleet(ai_settings, screen,ship,aliens) 
    #开始循环
    while True:
        #监听鼠标键盘数据
        gf.check_events(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets)
        #显示得分
        if stats.game_active:
     
          ship.update()  
        #每次关闭后重新绘制
          bullets.update()
        #删除已消失的子弹
          gf.remove_bullet(bullets)
          gf.update_bullets(ai_settings,screen,stats,sb,ship,aliens,bullets)
          gf.update_aliens(ai_settings,stats,sb,screen,ship,aliens,bullets)
        gf.update_screen(ai_settings , screen, stats,sb,ship, aliens, bullets, play_button)
run_game()

 