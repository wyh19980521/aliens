class GameStats():
    def __init__(self,ai_settings):
        self.ai_settings = ai_settings
        self.reset_starts()
        # 游戏刚启动时处于活动状态
        self.game_active = True
        self.high_score = 0 
        # 让游戏一开始处于非活动状态
        self.game_active = False
    def reset_starts(self):
        self.ships_left = self.ai_settings.ship_limit
        self.score = 0
        self.level =  1
        #为在每次开始游戏时都重置得分，我们在reset_stats()而不是__init__()中初始化score。

        '''
        在这个游戏运行期间，我们只创建一个GameStats实例，但每当玩家开始新游戏时，需要重
置一些统计信息。为此，我们在方法reset_stats()中初始化大部分统计信息，而不是在__init__()
中直接初始化它们。我们在__init__()中调用这个方法，这样创建GameStats实例时将妥善地设置
这些统计信息（见Ø），同时在玩家开始新游戏时也能调用reset_stats()。
当前只有一项统计信息——ships_left，其值在游戏运行期间将不断变化。一开始玩家拥有
的飞船数存储在settings.py的ship_limit中：
        '''