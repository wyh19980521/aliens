import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    '''一个对 飞船发射子弹管理类'''
    def __init__(self,ai_settings,screen,ship):
        super(Bullet,self).__init__()
        self.screen = screen
        #在0.0处生成一个子弹矩形，再设置正确的位置
        '''
        Bullet类继承了我们从模块pygame.sprite中导入的Sprite类。通过使用精灵，可将游戏中相
关的元素编组，进而同时操作编组中的所有元素。为创建子弹实例，需要向__init__()传递
ai_settings、screen和ship实例，还调用了super()来继承Sprite。
我们创建了子弹的属性rect。子弹并非基于图像的，因此我们必须使用pygame.Rect()
类从空白开始创建一个矩形。创建这个类的实例时，必须提供矩形左上角的x坐标和y坐标，还有
矩形的宽度和高度。我们在(0, 0)处创建这个矩形，但接下来的两行代码将其移到了正确的位置，
因为子弹的初始位置取决于飞船当前的位置。子弹的宽度和高度是从ai_settings中获取的。
在处，我们将子弹的centerx设置为飞船的rect.centerx。子弹应从飞船顶部射出，因此我
们将表示子弹的rect的top属性设置为飞船的rect的top属性，让子弹看起来像是从飞船中射出的
（见）。
我们将子弹的y坐标存储为小数值，以便能够微调子弹的速度（见）。在处，我们将子弹
的颜色和速度设置分别存储到self.color和self.speed_factor中。
下面是bullet.py的第二部分——方法update()和draw_bullet()：
        '''
        self.rect = pygame.Rect(0,0,ai_settings.bullet_width,ai_settings.bullet_height)
        self.rect.centerx=ship.rect.centerx
        self.rect.top=ship.rect.top
        #存储用小数标识位置
        self.y=float(self.rect.y)
        self.color=ai_settings.bullet_color
        self.speed_factor = ai_settings.bullet_speed_factor
    def update(self):
        self.y -= self.speed_factor
        self.rect.y=self.y
    def draw_bullet(self):
        pygame.draw.rect(self.screen,self.color,self.rect)